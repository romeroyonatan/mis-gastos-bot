MisGastosBot
=============

> Registra gastos de forma simple


Bot para telegram para registrar gastos. Util para llevar nuestra planificacion
financiera al día.


Ejecución
----------
Primero vas a necesitar hablar con @GodFather para pedir un token del bot y
guardarlo en un archivo `.token`


```sh
$ pip install -r requirements.txt
$ python -m expenses.telegram_bot
```

Docker
-------

```sh
docker-compose build
docker-compose up
```

Tests
----------
```
pytest
```
