import collections
import csv
import io
import logging
import os
import tempfile
from datetime import datetime

from telegram import Update
from telegram.constants import ParseMode
from telegram.ext import (
    ApplicationBuilder,
    CallbackContext,
    CommandHandler,
    filters,
    MessageHandler,
)

from expenses.systems import PersistentExpensesSystem

DATABASE = os.getenv("DBFILE", "expenses.db")
TOKENFILE = os.getenv("TOKENFILE", ".token")

logger = logging.getLogger(__name__)


def main():
    logging.basicConfig(
        format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
        level=logging.INFO,
    )
    logger.info("Inicializando bot")

    logger.info("Leyendo token en %r", TOKENFILE)
    with open(TOKENFILE) as token_file:
        token = token_file.read().strip()

    application = ApplicationBuilder().token(token).build()

    application.add_handler(CommandHandler("start", start))
    application.add_handler(CommandHandler("help", start))
    application.add_handler(CommandHandler("today", view_today))
    application.add_handler(CommandHandler("yesterday", view_yesterday))
    application.add_handler(CommandHandler("lastmonth", view_last_month))
    application.add_handler(CommandHandler("thismonth", view_this_month))
    application.add_handler(CommandHandler("exportlastmonth", export_last_month))
    application.add_handler(CommandHandler("exportthismonth", export_this_month))
    application.add_handler(CommandHandler("export", export_this_month))
    application.add_handler(
        MessageHandler(filters.TEXT & (~filters.COMMAND), register_expense)
    )

    application.run_polling()


async def start(update, context):
    await context.bot.send_message(
        chat_id=update.effective_chat.id,
        text="Hola! Podes registrar tus gastos así `gaste 100 pesos en queso y dulce`",
        parse_mode=ParseMode.MARKDOWN,
    )


async def register_expense(update, _):
    user = update.message.from_user
    message = update.message.text

    system = PersistentExpensesSystem.from_file(DATABASE)
    try:
        register = system.register(
            user=str(user.id), expense=message, at=datetime.utcnow(),
        )
    except ValueError as e:
        logger.error(
            f"Cannot parse message from user={user.full_name}(id={user.id}) message={message!r}"
        )
        await update.message.reply_text("Perdón, no entendí el mensaje")
    else:
        logger.info(f"User {user.full_name}(id={user.id}) registers an expense")
        await update.message.reply_text(
            f"Se registró un gasto por {register.amount} "
            f"{register.currency} en {register.description}.\n\n"
            f"Hoy gastaste {register.total_today} {register.currency}."
        )


async def view_today(update: Update, _):
    system = PersistentExpensesSystem.from_file(DATABASE)
    report = system.report_today(
        user=str(update.message.from_user.id), now=datetime.utcnow(),
    )
    await _send_report_as_text(report, update)


async def view_yesterday(update: Update, _):
    system = PersistentExpensesSystem.from_file(DATABASE)
    report = system.report_yesterday(
        user=str(update.message.from_user.id), now=datetime.utcnow(),
    )
    await _send_report_as_text(report, update)


async def view_last_month(update: Update, _):
    system = PersistentExpensesSystem.from_file(DATABASE)
    report = system.report_last_month(
        user=str(update.message.from_user.id), now=datetime.utcnow(),
    )
    await _send_report_as_text(report, update)


async def view_this_month(update: Update, _):
    system = PersistentExpensesSystem.from_file(DATABASE)
    report = system.report_this_month(
        user=str(update.message.from_user.id), now=datetime.utcnow(),
    )
    await _send_report_as_text(report, update)


async def export_last_month(update: Update, context: CallbackContext):
    system = PersistentExpensesSystem.from_file(DATABASE)
    report = system.report_last_month(
        user=str(update.message.from_user.id), now=datetime.utcnow(),
    )
    await _send_report_as_csv(report, update, context.bot)


async def export_this_month(update: Update, context: CallbackContext):
    system = PersistentExpensesSystem.from_file(DATABASE)
    report = system.report_this_month(
        user=str(update.message.from_user.id), now=datetime.utcnow(),
    )
    await _send_report_as_csv(report, update, context.bot)


async def _send_report_as_text(report, update):
    if not report:
        await update.message.reply_text("No hay gastos registrados")
        return

    totals = collections.Counter()
    message = io.StringIO()

    for register in report:
        totals[register.currency] += register.amount
        message.write(
            f"{register.amount:<4}{register.currency:3} {register.description}\n"
        )

    message.write("\nTotal\n")
    for currency, total in totals.items():
        message.write(f"{total}{currency}\n")

    await update.message.reply_text(message.getvalue())


async def _send_report_as_csv(report, update: Update, bot):
    if not report:
        await update.message.reply_text("No hay gastos registrados")
        return

    with tempfile.NamedTemporaryFile("w", suffix=".csv") as f:
        writer = csv.DictWriter(
            f, fieldnames=("Date", "Amount", "Currency", "Description")
        )
        writer.writeheader()
        writer.writerows(
            {
                "Date": register.date.date(),
                "Amount": float(register.amount),
                "Currency": register.currency,
                "Description": register.description,
            }
            for register in report
        )
        f.seek(0)
        await bot.send_document(
            chat_id=update.effective_chat.id, document=open(f.name, "rb"),
        )


if __name__ == "__main__":
    main()
