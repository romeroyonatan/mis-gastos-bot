import decimal
from dataclasses import FrozenInstanceError

import pytest

from expenses.systems import ExpensesSystem, Register
from expenses.currencies import EURO, PESO_ARGENTINO, DOLAR
from tests.bucket import (
    A_DATE,
    A_MONTH_AGO,
    A_YEAR_AGO,
    THIS_MONTH,
    NOW,
    TWO_MONTHS_AGO,
    JUAN_PEREZ,
    PEPE_SANCHEZ,
    YESTERDAY,
)


def test_there_are_not_expenses():
    system = ExpensesSystem()

    actual = system.report(JUAN_PEREZ)
    expected = []
    assert actual == expected


def test_register_one_expense():
    system = ExpensesSystem()

    system.register(JUAN_PEREZ, "gaste 10 euros en cena romantica", at=A_DATE)

    actual = system.report(JUAN_PEREZ)
    expected = [
        Register(
            date=A_DATE,
            description="Cena romantica",
            amount=decimal.Decimal(10),
            currency=EURO,
            user=JUAN_PEREZ,
        )
    ]
    assert actual == expected


@pytest.mark.parametrize("amount", ["0.5", "0.99", "9.99", "99.99", "999.99"])
def test_can_register_expenses_with_decimals(amount):
    system = ExpensesSystem()

    system.register(JUAN_PEREZ, f"gaste {amount} euros en cena romantica", at=A_DATE)

    actual = system.report(JUAN_PEREZ)
    expected = [
        Register(
            date=A_DATE,
            description="Cena romantica",
            amount=decimal.Decimal(amount),
            currency=EURO,
            user=JUAN_PEREZ,
        )
    ]
    assert actual == expected


def test_can_register_words_with_acutes():
    system = ExpensesSystem()

    system.register(JUAN_PEREZ, "gasté 10 dólares en ñandúes", at=A_DATE)

    actual = system.report(JUAN_PEREZ)
    expected = [
        Register(
            date=A_DATE,
            description="Ñandúes",
            amount=decimal.Decimal(10),
            currency=DOLAR,
            user=JUAN_PEREZ,
        )
    ]
    assert actual == expected


def test_can_register_capitalized_expressions():
    system = ExpensesSystem()

    system.register(JUAN_PEREZ, "Gasté 10 dólares en ñandúes", at=A_DATE)

    actual = system.report(JUAN_PEREZ)
    expected = [
        Register(
            date=A_DATE,
            description="Ñandúes",
            amount=decimal.Decimal(10),
            currency=DOLAR,
            user=JUAN_PEREZ,
        )
    ]
    assert actual == expected


def test_when_currency_is_not_specified_it_should_use_the_default_currency():
    system = ExpensesSystem(default_currency=EURO)

    system.register(JUAN_PEREZ, "gaste 10 en cena romantica", at=A_DATE)

    actual = system.report(JUAN_PEREZ)
    expected = [
        Register(
            date=A_DATE,
            description="Cena romantica",
            amount=decimal.Decimal(10),
            currency=EURO,
            user=JUAN_PEREZ,
        )
    ]
    assert actual == expected


def test_when_currency_is_not_specified_it_should_use_the_default_currency():
    system = ExpensesSystem(default_currency=EURO)

    system.register(JUAN_PEREZ, "gaste 10 en cena romantica", at=A_DATE)

    actual = system.report(JUAN_PEREZ)
    expected = [
        Register(
            date=A_DATE,
            description="Cena romantica",
            amount=decimal.Decimal(10),
            currency=EURO,
            user=JUAN_PEREZ,
        )
    ]
    assert actual == expected


def test_can_use_spaces_before_and_after_expression():
    system = ExpensesSystem()

    system.register(JUAN_PEREZ, "  gaste 2000 pesos en cena romantica  ", at=A_DATE)

    actual = system.report(JUAN_PEREZ)
    expected = [
        Register(
            date=A_DATE,
            description="Cena romantica",
            amount=decimal.Decimal(2000),
            currency=PESO_ARGENTINO,
            user=JUAN_PEREZ,
        )
    ]
    assert actual == expected


@pytest.mark.parametrize(
    "currency_name, currency",
    [("peso", PESO_ARGENTINO), ("euro", EURO), ("dolar", DOLAR), ("dólar", DOLAR)],
)
def test_can_use_singular_names(currency_name, currency):
    system = ExpensesSystem()
    system.register(JUAN_PEREZ, f"gaste 1 {currency_name} en 1 pizza", at=A_DATE)

    actual = system.report(JUAN_PEREZ)
    expected = [
        Register(
            date=A_DATE,
            description="1 pizza",
            amount=decimal.Decimal(1),
            currency=currency,
            user=JUAN_PEREZ,
        )
    ]
    assert actual == expected


@pytest.mark.parametrize(
    "currency_name, currency",
    [
        ("pesos", PESO_ARGENTINO),
        ("euros", EURO),
        ("dolares", DOLAR),
        ("dólares", DOLAR),
    ],
)
def test_can_use_plural_names(currency_name, currency):
    system = ExpensesSystem()
    system.register(JUAN_PEREZ, f"gaste 2 {currency_name} en dos pizzas", at=A_DATE)

    actual = system.report(JUAN_PEREZ)
    expected = [
        Register(
            date=A_DATE,
            description="Dos pizzas",
            amount=decimal.Decimal(2),
            currency=currency,
            user=JUAN_PEREZ,
        )
    ]
    assert actual == expected


@pytest.mark.parametrize(
    "expression",
    [
        "",
        "gaste -1 euro en pizza",
        "gaste 1 dolar",
        "gaste 1 dolar en",
        "gaste 1 dolar en  ",
        "gaste 1 dolar en 1 ",
    ],
)
def test_invalid_expressions(expression):
    system = ExpensesSystem()

    with pytest.raises(ValueError, match=f"Cannot parse expression: {expression!r}"):
        system.register(JUAN_PEREZ, expression, at=A_DATE)
    assert system.report(JUAN_PEREZ) == []


def test_can_register_more_than_one_expense():
    system = ExpensesSystem()

    system.register(JUAN_PEREZ, "gaste 20 euros en cena romantica", at=A_DATE)
    system.register(JUAN_PEREZ, "gaste 1 euro en helado", at=A_DATE)

    actual = system.report(JUAN_PEREZ)
    expected = [
        Register(
            date=A_DATE,
            description="Cena romantica",
            amount=decimal.Decimal(20),
            currency=EURO,
            user=JUAN_PEREZ,
        ),
        Register(
            date=A_DATE,
            description="Helado",
            amount=decimal.Decimal(1),
            currency=EURO,
            user=JUAN_PEREZ,
        ),
    ]
    assert actual == expected


def test_registers_cannot_be_modified_from_external_sources():
    system = ExpensesSystem()

    system.register(JUAN_PEREZ, "gaste 20 euros en cena romantica", at=A_DATE)
    report = system.report(JUAN_PEREZ)
    with pytest.raises(FrozenInstanceError):
        report[0].amount = 1

    actual = system.report(JUAN_PEREZ)
    expected = [
        Register(
            date=A_DATE,
            description="Cena romantica",
            amount=decimal.Decimal(20),
            currency=EURO,
            user=JUAN_PEREZ,
        ),
    ]
    assert actual == expected


def test_handle_multiple_users():
    system = ExpensesSystem()

    system.register(JUAN_PEREZ, "gaste 2 euros en vespa", at=A_DATE)
    system.register(PEPE_SANCHEZ, "gaste 10 dolares en trencito", at=A_DATE)

    actual = system.report(JUAN_PEREZ)
    expected = [
        Register(
            date=A_DATE,
            description="Vespa",
            amount=decimal.Decimal(2),
            currency=EURO,
            user=JUAN_PEREZ,
        ),
    ]
    assert actual == expected

    actual = system.report(PEPE_SANCHEZ)
    expected = [
        Register(
            date=A_DATE,
            description="Trencito",
            amount=decimal.Decimal(10),
            currency=DOLAR,
            user=PEPE_SANCHEZ,
        ),
    ]
    assert actual == expected


def test_get_last_month_expenses():
    system = ExpensesSystem()

    system.register(JUAN_PEREZ, "gaste 99 euros en pizza", at=TWO_MONTHS_AGO)
    system.register(JUAN_PEREZ, "gaste 1 euro en pizza", at=A_MONTH_AGO)
    system.register(JUAN_PEREZ, "gaste 2 euros en pizza", at=THIS_MONTH)

    actual = system.report_last_month(JUAN_PEREZ, now=THIS_MONTH)
    expected = [
        Register(
            date=A_MONTH_AGO,
            description="Pizza",
            amount=decimal.Decimal(1),
            currency=EURO,
            user=JUAN_PEREZ,
        ),
    ]
    assert actual == expected


def test_get_current_month_expenses():
    system = ExpensesSystem()

    system.register(JUAN_PEREZ, "gaste 1 euro en pizza", at=A_YEAR_AGO)
    system.register(JUAN_PEREZ, "gaste 1 euro en pizza", at=TWO_MONTHS_AGO)
    system.register(JUAN_PEREZ, "gaste 1 euro en pizza", at=A_MONTH_AGO)
    system.register(JUAN_PEREZ, "gaste 1 euro en pizza", at=THIS_MONTH)

    actual = system.report_this_month(JUAN_PEREZ, now=THIS_MONTH)
    expected = [
        Register(
            date=THIS_MONTH,
            description="Pizza",
            amount=decimal.Decimal(1),
            currency=EURO,
            user=JUAN_PEREZ,
        ),
    ]
    assert actual == expected


def test_cannot_month_expenses_of_another_user():
    system = ExpensesSystem()

    system.register(JUAN_PEREZ, "gaste 1 euro en pizza", at=A_MONTH_AGO)
    system.register(JUAN_PEREZ, "gaste 2 euros en pizza", at=THIS_MONTH)

    actual = system.report_this_month(PEPE_SANCHEZ, now=THIS_MONTH)
    expected = []
    assert actual == expected


def test_get_specified_month_expenses():
    system = ExpensesSystem()

    system.register(JUAN_PEREZ, "gaste 1 euro en pizza", at=A_YEAR_AGO)
    system.register(JUAN_PEREZ, "gaste 1 euro en pizza", at=TWO_MONTHS_AGO)
    system.register(JUAN_PEREZ, "gaste 1 euro en pizza", at=A_MONTH_AGO)
    system.register(JUAN_PEREZ, "gaste 1 euro en pizza", at=THIS_MONTH)

    actual = system.report_expenses_made_by_user_in_same_month_of(
        user=JUAN_PEREZ, a_date=A_YEAR_AGO.date()
    )
    expected = [
        Register(
            date=A_YEAR_AGO,
            description="Pizza",
            amount=decimal.Decimal(1),
            currency=EURO,
            user=JUAN_PEREZ,
        ),
    ]
    assert actual == expected


def test_get_today_expenses():
    system = ExpensesSystem()

    system.register(JUAN_PEREZ, "gaste 1 euro en pizza", at=THIS_MONTH)
    system.register(JUAN_PEREZ, "gaste 1 euro en pizza", at=NOW)

    actual = system.report_today(user=JUAN_PEREZ, now=NOW)
    expected = [
        Register(
            date=NOW,
            description="Pizza",
            amount=decimal.Decimal(1),
            currency=EURO,
            user=JUAN_PEREZ,
        ),
    ]
    assert actual == expected


def test_get_yesterday_expenses():
    system = ExpensesSystem()

    system.register(JUAN_PEREZ, "gaste 1 euro en pizza", at=YESTERDAY)
    system.register(JUAN_PEREZ, "gaste 2 euros en pizza", at=NOW)

    actual = system.report_yesterday(user=JUAN_PEREZ, now=NOW)
    expected = [
        Register(
            date=YESTERDAY,
            description="Pizza",
            amount=decimal.Decimal(1),
            currency=EURO,
            user=JUAN_PEREZ,
        ),
    ]
    assert actual == expected


def test_get_total_expenses_today():
    system = ExpensesSystem()

    registered_expense_1 = system.register(
        JUAN_PEREZ, "gaste 450 euros en hotel", at=YESTERDAY
    )
    registered_expense_1 = system.register(
        JUAN_PEREZ, "gaste 10 euros en pizza", at=NOW
    )
    assert registered_expense_1.total_today == 10

    registered_expense_2 = system.register(
        JUAN_PEREZ, "gaste 5 euros en cerveza", at=NOW
    )
    assert registered_expense_2.total_today == 15

    registered_expense_3 = system.register(JUAN_PEREZ, "gaste 1 euro en propina", at=NOW)
    assert registered_expense_3.total_today == 16


def test_the_total_today_expenses_are_only_for_the_current_currency():
    system = ExpensesSystem()

    registered_expense_1 = system.register(
        JUAN_PEREZ, "gaste 10 euros en pizza", at=NOW
    )
    assert registered_expense_1.total_today == 10

    registered_expense_2 = system.register(
        JUAN_PEREZ, "gaste 2500 pesos en cerveza", at=NOW
    )
    assert registered_expense_2.total_today == 2500

    registered_expense_3 = system.register(JUAN_PEREZ, "gaste 1 euro en propina", at=NOW)
    assert registered_expense_3.total_today == 11


def test_the_total_today_expenses_are_only_for_the_current_user():
    system = ExpensesSystem()

    registered_expense_1 = system.register(
        JUAN_PEREZ, "gaste 10 euros en pizza", at=NOW
    )
    assert registered_expense_1.total_today == 10

    registered_expense_2 = system.register(
        PEPE_SANCHEZ, "gaste 5 euros en cerveza", at=NOW
    )
    assert registered_expense_2.total_today == 5
