import datetime

JUAN_PEREZ = "Juan Pérez"
PEPE_SANCHEZ = "Pepe sanchez"

A_YEAR_AGO = datetime.datetime(1969, 11, 1, 0, 0, 0, tzinfo=datetime.timezone.utc)
TWO_MONTHS_AGO = datetime.datetime(1970, 9, 1, 0, 0, 0, tzinfo=datetime.timezone.utc)
A_MONTH_AGO = datetime.datetime(1970, 10, 1, 0, 0, 0, tzinfo=datetime.timezone.utc)
THIS_MONTH = datetime.datetime(1970, 11, 1, 0, 0, 0, tzinfo=datetime.timezone.utc)

A_DATE = datetime.datetime(1970, 1, 1, 0, 0, 0, tzinfo=datetime.timezone.utc)
YESTERDAY = datetime.datetime(1970, 11, 1, 3, 20, 25, tzinfo=datetime.timezone.utc)
NOW = datetime.datetime(1970, 11, 2, 3, 20, 25, tzinfo=datetime.timezone.utc)

