import dataclasses
import datetime
import re
from decimal import Decimal
from pathlib import Path
from typing import List, Iterable, Union

from dateutil.relativedelta import relativedelta
from tinydb import TinyDB

from expenses.currencies import EURO, CURRENCY_NAMES


def _compile_expression_regex():
    currencies = "|".join(sorted(CURRENCY_NAMES, key=lambda x: len(x), reverse=True))
    regex = (
        r"gast\w\s*"
        r"(?P<amount>\d+(.\d{1,2})?)\s*"
        rf"(?P<currency>{currencies})?\s*"
        r"en\s*"
        r"(?P<description>\w.+)$"
    )
    return re.compile(regex, flags=re.IGNORECASE)


@dataclasses.dataclass(frozen=True)
class ExpenseRegisteredSucceed:
    """I am a registered expense"""

    description: str
    currency: str
    amount: Decimal
    # sum of all today's expenses in with the same currency
    total_today: Decimal


class ExpensesSystem:
    EXPRESSION_REGEX = _compile_expression_regex()
    DEFAULT_CURRENCY = EURO

    def __init__(self, default_currency=DEFAULT_CURRENCY):
        self.default_currency = default_currency
        self._registers = []

    def register(
        self, user: str, expense: str, at: datetime.datetime
    ) -> ExpenseRegisteredSucceed:
        """Register a new expense

        Args:
            user: the user which register the expense
            expense: Spanish expression of the expense. For example
                     "gaste 1 euro en pizza".
            at: When the expense was made.

        Raises:
            ValueError: The expression was not understood
        """
        match = self._parse_expression(expense.strip())
        self._assert_expression_is_valid(expense, match)
        register = self._build_register(at, match, user)
        self._add_register(register)
        total_today = self._get_total_expeneses_today(register.currency, register.user, at)
        return ExpenseRegisteredSucceed(
            description=register.description,
            currency=register.currency,
            amount=register.amount,
            total_today=total_today,
        )

    def _parse_expression(self, expression):
        return ExpensesSystem.EXPRESSION_REGEX.search(expression)

    def _assert_expression_is_valid(self, expression, match):
        if not match:
            raise ValueError(f"Cannot parse expression: {expression!r}")

    def _build_register(self, a_date: datetime.datetime, match: re.Match, user: str):
        a_dict = match.groupdict()
        description = a_dict["description"].capitalize()
        amount = Decimal(a_dict["amount"])
        currency = self._get_currency(a_dict.get("currency"))
        register = Register(user, a_date, description, amount, currency)
        return register

    def _add_register(self, register):
        self._registers.append(register)

    def _get_total_expeneses_today(self, currency: str, user: str, at: datetime):
        return sum(
            register.amount
            for register in self.report_today(user, at)
            if register.currency == currency
        )

    def _get_currency(self, currency_name):
        if not currency_name:
            return self.default_currency
        return CURRENCY_NAMES[currency_name]

    def report(self, user: str) -> List["Register"]:
        return [register for register in self._get_registers_of_user(user)]

    def _get_registers_of_user(self, user: str):
        return sorted(
            (register for register in self._registers if register.is_of_user(user)),
            key=lambda x: x.date,
        )

    def report_last_month(self, user: str, now: datetime.datetime) -> List["Register"]:
        last_month = now - relativedelta(months=1)
        return self.report_expenses_made_by_user_in_same_month_of(
            user, last_month.date()
        )

    def report_this_month(self, user: str, now: datetime.datetime) -> List["Register"]:
        return self.report_expenses_made_by_user_in_same_month_of(user, now.date())

    def report_expenses_made_by_user_in_same_month_of(
        self, user: str, a_date: datetime.date
    ) -> List["Register"]:
        return [
            register
            for register in self._get_registers_of_user(user)
            if register.was_made_in_same_month_of(a_date)
        ]

    def report_today(self, user, now):
        return [
            register
            for register in self._get_registers_of_user(user)
            if register.was_made_in_same_day_of(now.date())
        ]

    def report_yesterday(self, user, now):
        yesterday = now - relativedelta(days=1)
        return [
            register
            for register in self._get_registers_of_user(user)
            if register.was_made_in_same_day_of(yesterday.date())
        ]


@dataclasses.dataclass(frozen=True)
class Register:
    user: str
    date: datetime.datetime
    description: str
    amount: Decimal
    currency: str

    @classmethod
    def from_dict(cls, a_dict: dict) -> "Register":
        return cls(
            date=datetime.datetime.fromisoformat(a_dict["date"]),
            description=a_dict["description"],
            amount=Decimal(a_dict["amount"]),
            user=a_dict["user"],
            currency=a_dict["currency"],
        )

    def as_dict(self):
        return {
            "date": self.date.isoformat(),
            "description": self.description,
            "amount": str(self.amount),
            "user": self.user,
            "currency": self.currency,
        }

    def is_of_user(self, user: str) -> bool:
        return self.user == user

    def was_made_in_same_month_of(self, a_date: datetime.date) -> bool:
        return self.date.month == a_date.month and self.date.year == a_date.year

    def was_made_in_same_day_of(self, a_date: datetime.date) -> bool:
        return (
            self.date.day == a_date.day
            and self.date.month == a_date.month
            and self.date.year == a_date.year
        )


class PersistentExpensesSystem(ExpensesSystem):
    """Expense system which stores the state in a tinydb file"""

    def __init__(self, registers: Iterable[Register], db: TinyDB, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._db = db
        self._registers = list(registers)

    @classmethod
    def from_file(
        cls,
        file_path: Union[Path, str],
        default_currency=ExpensesSystem.DEFAULT_CURRENCY,
    ) -> ExpensesSystem:
        db = TinyDB(file_path)
        registers = [Register.from_dict(row) for row in db]
        return cls(registers=registers, db=db, default_currency=default_currency)

    def _add_register(self, register):
        super()._add_register(register)
        self._db.insert(register.as_dict())
