from decimal import Decimal

from expenses.currencies import EURO
from expenses.systems import PersistentExpensesSystem, Register
from tests.bucket import JUAN_PEREZ, A_DATE, PEPE_SANCHEZ

AN_EXPRESSION = "gaste 9.99 euros en pizza con cervezas"
JUAN_PEREZ_REGISTER = Register(
    date=A_DATE,
    description="Pizza con cervezas",
    amount=Decimal("9.99"),
    currency=EURO,
    user=JUAN_PEREZ,
)
PEPE_SANCHEZ_REGISTER = Register(
    date=A_DATE,
    description="Pizza con cervezas",
    amount=Decimal("9.99"),
    currency=EURO,
    user=PEPE_SANCHEZ,
)


def test_a_new_persistent_system_is_empty(tmp_path):
    db = tmp_path / "expenses.db"

    system = PersistentExpensesSystem.from_file(db)

    actual = system.report(JUAN_PEREZ)
    expected = []
    assert actual == expected


def test_persistent_system_should_read_state_from_db_file(tmp_path):
    db = tmp_path / "expenses.db"

    system1 = PersistentExpensesSystem.from_file(db)
    system1.register(JUAN_PEREZ, AN_EXPRESSION, A_DATE)

    system2 = PersistentExpensesSystem.from_file(db)
    system2.register(PEPE_SANCHEZ, AN_EXPRESSION, A_DATE)

    system3 = PersistentExpensesSystem.from_file(db)

    actual = system3.report(JUAN_PEREZ)
    expected = [JUAN_PEREZ_REGISTER]
    assert actual == expected

    actual = system3.report(PEPE_SANCHEZ)
    expected = [PEPE_SANCHEZ_REGISTER]
    assert actual == expected
