FROM python:3.12-slim

ENV TOKENFILE=/opt/mis-gastos-bot/.token \
    DBFILE=/opt/mis-gastos-bot/expenses.db

WORKDIR /app

ADD requirements.txt .
RUN python -m pip install -r requirements.txt

ADD pyproject.toml .
ADD src src/
RUN pip install .

VOLUME /opt/mis-gastos-bot

USER 65534
CMD ["python", "-m", "expenses.telegram_bot"]
