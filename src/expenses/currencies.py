DOLAR = "USD"
EURO = "EUR"
PESO_ARGENTINO = "ARS"

CURRENCY_NAMES = {
    "pesos": PESO_ARGENTINO,
    "peso": PESO_ARGENTINO,
    "euros": EURO,
    "euro": EURO,
    "dolar": DOLAR,
    "dólar": DOLAR,
    "dolares": DOLAR,
    "dólares": DOLAR,
}
